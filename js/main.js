(
  function () {
    var currentIndex = 0;

    var imgList = [
      { id: 1, img: 'one' },
      { id: 2, img: 'two' },
      { id: 3, img: 'three' },
      { id: 4, img: 'four' },
      { id: 5, img: 'five' },
      { id: 6, img: 'six' },
      { id: 7, img: 'seven' },
      { id: 8, img: 'eight' },
      { id: 9, img: 'nine' },
      { id: 10, img: 'ten' },
      { id: 11, img: 'eleven' },
      { id: 12, img: 'twelve' }
    ];

    function generateThumbNails() {
      var tnParent = document.querySelector('[data-id="thumbnails"]');
      if (tnParent.hasChildNodes()) {
        for (var i = 0; i < tnParent.childNodes.length; i++) {
          var thumb = tnParent.childNodes[i];
          tnParent.removeChild(thumb);
        };
      }
      for (var index = 0; index < imgList.length; index++) {
        var thumb = imgList[index];
        var thumbEle = document.createElement('span');
        //<span class="col-1 col-xs-1 thumbnail one"></span>
        thumbEle.className = 'col-1 col-xs-1 thumbnail thumb-' + thumb.img;
        thumbEle.setAttribute("data-index", thumb.id);

        thumbEle.addEventListener('click', thumbClickHandler);

        tnParent.appendChild(thumbEle);
      };

    }

    function thumbClickHandler(event) {
      var imgIndex = (event.target).getAttribute("data-index");
      currentIndex = parseInt(imgIndex) - 1;
      var largePicEle = document.querySelector('[data-id="large-pic"]');
      largePicEle.className = filterClass(largePicEle.className, imgList[currentIndex].img);
      highlightThumb();
    }

    function filterClass(imgClass, value) {
      var index = imgClass.indexOf('large');
      if (index !== -1) {
        return imgClass.substr(0, index) + ' large-' + value;
      }
      return imgClass + ' large-' + value;
    }

    function InitiateListeners() {

      var showPrev = document.querySelector('[data-id="showPrev"]');
      var showNext = document.querySelector('[data-id="showNext"]');

      showPrev.addEventListener('click', prevClickHandler);
      showNext.addEventListener('click', nextClickHandler);
    }

    function nextClickHandler(event) {
      currentIndex = currentIndex === imgList.length - 1 ? 0 : currentIndex;
      var largePicEle = document.querySelector('[data-id="large-pic"]');
      largePicEle.className = filterClass(largePicEle.className, imgList[++currentIndex].img);
      highlightThumb();
    }

    function prevClickHandler(event) {
      currentIndex = currentIndex === 0 ? imgList.length - 1 : currentIndex;
      var largePicEle = document.querySelector('[data-id="large-pic"]');
      largePicEle.className = filterClass(largePicEle.className, imgList[--currentIndex].img);
      highlightThumb();
    }

    function highlightThumb() {
      //highlight
      var thumb = document.querySelector('.thumb-' + imgList[currentIndex].img);
      var tnParent = document.querySelector('[data-id="thumbnails"]');
      if (tnParent.hasChildNodes()) {
        for (var i = 0; i < tnParent.childNodes.length; i++) {
          var thumb = tnParent.childNodes[i];
          thumb.nextSibling = thumb.nextSibling || {};
          thumb.nextSibling.className = thumb.nextSibling.className || '';
          if (i === currentIndex) {
            thumb.nextSibling.className = thumb.nextSibling.className + ' highlight';
          } else if (thumb.nextSibling) {
            thumb.nextSibling.className = thumb.nextSibling.className.replace('highlight', '');
          }
        };
      }

    }

    generateThumbNails();
    InitiateListeners();
  }
)();